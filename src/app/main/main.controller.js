(function() {
    'use strict';

    angular
        .module('devices')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($scope, GAuth, GData, $state, $rootScope, $firebaseArray) {
        var scope = this;
        var ref = new Firebase("https://devicescontrol.firebaseio.com/devices");
        scope.cloudDevices = $firebaseArray(ref);

        if(GData.isLogin()){
            //$state.go('home');
        };

        scope.doLogin = function() {
            GAuth.login().then(function(){
                //$state.go('home');
            });
        };

        scope.doLogout = function(){
            $rootScope.logout();
        };

        scope.setUser = function(device,id){
            scope.cloudDevices[id].user = $rootScope.gapi.user;
            scope.cloudDevices.$save(id);
        };

        scope.backDevice = function(id){
            delete scope.cloudDevices[id].user;
            scope.cloudDevices.$save(id);
        }
    }
})();