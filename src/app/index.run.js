(function() {
  'use strict';

  angular
    .module('devices')
    .run(runBlock)
    .run(['GAuth', 'GApi', '$state', '$rootScope', '$window',
     function(GAuth, GApi, $state, $rootScope, $window) {

         var CLIENT = '248665137536-4bcr3e2634lb04unrbtg63qvruhful9r.apps.googleusercontent.com';
         var BASE;
         if($window.location.hostname === 'localhost') {
             BASE = 'http://localhost:8080/_ah/api';
         } else {
             BASE = 'https://cloud-endpoints-gae.appspot.com/_ah/api';
         }

         //GApi.load('myContactApi', 'v1', BASE);
         //GApi.load('calendar', 'v3');
         GAuth.setClient(CLIENT);
         GAuth.setScope('https://www.googleapis.com/auth/userinfo.email');
         GAuth.checkAuth().then(
             function(){
             //
             }
         );

         $rootScope.logout = function() {
             GAuth.logout().then(
                 function(){
                    //
                 }
             );
         };
     }
    ]);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }
})();
