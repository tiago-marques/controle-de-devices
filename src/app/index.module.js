(function() {
  'use strict';

  angular
      .module('devices', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ui.bootstrap','angular-google-gapi','firebase']);

})();
